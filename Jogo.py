import numpy as np
from copy import deepcopy

class Jogo:
    def __init__(self, jogador_ativo):
        self.tabuleiro = np.zeros((3,3))#quadro inicial // cada lista é uma linha
        self.jogador_ativo = jogador_ativo #o primeiro é pra mandar 1
        self.fim_do_jogo = False #o primeiro é pra mandar 1
        self.jogador_vencedor = 0 #o primeiro é pra mandar 1
        #self.jogadas_possiveis = None

    def jogada(self, posicao):
            
        if self.tabuleiro[posicao[0]][posicao[1]] != 0 :
            print(" JOGADA INVALIDA ")
            return self
        if self.fim_do_jogo:
            print(" JA ACABO ! ")
            return self

        _self = deepcopy(self)

        _self.tabuleiro[posicao[0]][posicao[1]] = _self.jogador_ativo
        _self.checarVencedor()
        _self.jogador_ativo *= -1
        return _self
            
    
    def checarVencedor(self):
        # linha 1 com jogador_ativo vencendo
        if sum(self.tabuleiro[0]) == 3 * self.jogador_ativo:
            self.fim_do_jogo = True
            self.jogador_vencedor = self.jogador_ativo
            return
        # linha 2 com jogador_ativo vencendo
        if sum(self.tabuleiro[1]) == 3 * self.jogador_ativo:
            self.fim_do_jogo = True
            self.jogador_vencedor = self.jogador_ativo
            return
        # linha 3 com jogador_ativo vencendo
        if sum(self.tabuleiro[2]) == 3 * self.jogador_ativo:
            self.fim_do_jogo = True
            self.jogador_vencedor = self.jogador_ativo
            return
        # coluna 1 com jogador_ativo vencendo
        if self.tabuleiro[0][0] + self.tabuleiro[1][0] + self.tabuleiro[2][0] == 3 * self.jogador_ativo:
            self.fim_do_jogo = True
            self.jogador_vencedor = self.jogador_ativo
            return
        # coluna 2 com jogador_ativo vencendo
        if self.tabuleiro[0][1] + self.tabuleiro[1][1] + self.tabuleiro[2][1] == 3 * self.jogador_ativo:
            self.fim_do_jogo = True
            self.jogador_vencedor = self.jogador_ativo
            return
        # coluna 3 com jogador_ativo vencendo
        if self.tabuleiro[0][2] + self.tabuleiro[1][2] + self.tabuleiro[2][2] == 3 * self.jogador_ativo:
            self.fim_do_jogo = True
            self.jogador_vencedor = self.jogador_ativo
            return
        # diagonal descendo com jogador_ativo vencendo
        if self.tabuleiro[0][0] + self.tabuleiro[1][1] + self.tabuleiro[2][2] == 3 * self.jogador_ativo:
            self.fim_do_jogo = True
            self.jogador_vencedor = self.jogador_ativo
            return
        # diagonal subindo com jogador_ativo vencendo
        if self.tabuleiro[2][0] + self.tabuleiro[1][1] + self.tabuleiro[0][2] == 3 * self.jogador_ativo:
            self.fim_do_jogo = True
            self.jogador_vencedor = self.jogador_ativo
            return
        
        if (self.tabuleiro != 0).all():
            self.fim_do_jogo = True
            self.jogador_vencedor = 0
            return 
    
    def printaTabuleiro(self):
        #print( str(self.tabuleiro) )
        print("------------------------------------------------------------------------------------------------------------------------------------------------------")
        print("\n                                                                JOGO DA VELHA!\n")
        print("                                                            0         1          2     ")
        print("                                                                 |          |          ")
        print("                                                                 |          |          ")
        print("                                                                 |          |          ")
        linha = [" "," "," "]
        if( self.tabuleiro[0][0] == 1 ):
            linha[0] = "X"
        elif(self.tabuleiro[0][0] == -1):
            linha[0] = "O"
        if( self.tabuleiro[0][1] == 1 ):
            linha[1] = "X"
        elif(self.tabuleiro[0][1] == -1):
            linha[1] = "O"
        if( self.tabuleiro[0][2] == 1 ):
            linha[2] = "X"
        elif(self.tabuleiro[0][2] == -1):
            linha[2] = "O"    
        print("                                                0          %s     |    %s     |     %s    " % (linha[0],linha[1],linha[2]) )
        print("                                                                 |          |          ")
        print("                                                                 |          |          ")
        print("                                                       ----------|----------|----------")
        print("                                                                 |          |          ")
        print("                                                                 |          |          ")
        print("                                                                 |          |          ")
        linha = [" "," "," "]
        if( self.tabuleiro[1][0] == 1 ):
            linha[0] = "X"
        elif(self.tabuleiro[1][0] == -1):
            linha[0] = "O"
        if( self.tabuleiro[1][1] == 1 ):
            linha[1] = "X"
        elif(self.tabuleiro[1][1] == -1):
            linha[1] = "O"
        if( self.tabuleiro[1][2] == 1 ):
            linha[2] = "X"
        elif(self.tabuleiro[1][2] == -1):
            linha[2] = "O"    
        print("                                                1          %s     |    %s     |     %s    " % (linha[0],linha[1],linha[2]) )
        print("                                                                 |          |          ")
        print("                                                                 |          |          ")
        print("                                                       ----------|----------|----------")
        print("                                                                 |          |          ")
        print("                                                                 |          |          ")
        linha = [" "," "," "]
        if( self.tabuleiro[2][0] == 1 ):
            linha[0] = "X"
        elif(self.tabuleiro[2][0] == -1):
            linha[0] = "O"
        if( self.tabuleiro[2][1] == 1 ):
            linha[1] = "X"
        elif(self.tabuleiro[2][1] == -1):
            linha[1] = "O"
        if( self.tabuleiro[2][2] == 1 ):
            linha[2] = "X"
        elif(self.tabuleiro[2][2] == -1):
            linha[2] = "O"    
        print("                                                2          %s     |    %s     |     %s    " % (linha[0],linha[1],linha[2]) )
        print("                                                                 |          |          ")
        print("                                                                 |          |          ")
        print("                                                                 |          |          ")
    