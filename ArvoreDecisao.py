from No import No
import numpy as np
from Jogo import Jogo



class ArvoreDecisao:
    def __init__(self):
        self.no_inicial = No(0)
        self.n_nos=1
        self.no_atual = self.no_inicial

    def criaArvoreDeDecisao(self):
        jogo = Jogo(1)

        self.preencheTabuleiro(jogo, self.no_inicial, 0)
        print('pronto')

    def descer(self, jogada):
        for no in self.no_atual.lista_nos_filhos:
            if no.jogada == jogada:
                self.no_atual = no

    def escolherJogada(self):
        melhor_jogada = self.no_atual.melhor.jogada
        self.descer(melhor_jogada)
        return melhor_jogada

    def preencheTabuleiro(self, jogo, no, k):

        if jogo.fim_do_jogo:
            no.peso = jogo.jogador_vencedor/no.camada
            return no.peso

        i = 0
        j = 0
        for i in range(3):
            for j in range(3):
                if jogo.tabuleiro[i, j] == 0:
                    novo_jogo = jogo.jogada((i, j))
                    filho = No(no.camada + 1, (i, j))
                    self.n_nos+=1
                    no.lista_nos_filhos.append(filho)
                    self.preencheTabuleiro(novo_jogo, filho, k+1)

                    if no.melhor == None:
                        no.melhor = filho
                    else:
                        if jogo.jogador_ativo > 0:
                            if filho.peso > no.melhor.peso:
                                no.melhor = filho
                        else:
                            if filho.peso < no.melhor.peso:
                                no.melhor = filho

        no.peso = no.melhor.peso
