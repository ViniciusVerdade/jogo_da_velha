import random
import numpy as np
import asciimatics
from random import randint
from asciimatics.screen import Screen
from asciimatics.scene import Scene
from asciimatics.effects import Cycle, Stars
from asciimatics.renderers import FigletText
from Jogo import Jogo
from No import No
from ArvoreDecisao import ArvoreDecisao
import os

def main():
    _jogador_inicial = input("Digite quem irá iniciar a partida, humano (H) ou máquina (M)?: ")
    if _jogador_inicial == 'H':
        jogador_inicial = 1
    elif _jogador_inicial == "M":
        jogador_inicial = -1 
    else:
        print("Opção inválida!")
        return
    
    jogo = Jogo(1)
#    jogo.printaTabuleiro()
    
    IA = ArvoreDecisao()
    print('Carregando...')
    IA.criaArvoreDeDecisao()
    print('Carregado!')

    print(IA.n_nos)

    if jogador_inicial < 0:
        melhor_jogada = IA.escolherJogada()
        IA.descer(melhor_jogada)
        jogo = jogo.jogada(melhor_jogada)
        
    while(True):
        # Vez do player
        jogo.printaTabuleiro()
        #print('Faz tua play')
        print("\n\n\nFaz a tua play( na formatação: 1,1): ")
        melhor_jogada = input().split(",")
        melhor_jogada = (int(melhor_jogada[0]), int(melhor_jogada[1]))
        jogo_possivel = jogo.jogada(melhor_jogada)
        if jogo_possivel != jogo:
            
            jogo = jogo_possivel
            IA.descer(melhor_jogada)
            print("\n\n\n")
            #print("------------------------------------------------------------------------------------------------------------------------------------------------------")
            
            if jogo.fim_do_jogo:
                jogo.printaTabuleiro()
                if jogo.jogador_vencedor == -1 and _jogador_inicial == "H":
                    print("Você Ganhou!")
                if jogo.jogador_vencedor == -1 and _jogador_inicial == "M":
                    print("A Máquina Ganhou! (Cuidado)")
                if jogo.jogador_vencedor == 1 and _jogador_inicial == "H":
                    print("Você Ganhou!")
                if jogo.jogador_vencedor == 1 and _jogador_inicial == "M":
                    print("A Máquina Ganhou! (Cuidado)")
                if jogo.jogador_vencedor == 0:
                    print("EMPATE!!")
                return

            # Vez da IA
            melhor_jogada = IA.escolherJogada()
            jogo = jogo.jogada(melhor_jogada)
            IA.descer(melhor_jogada)

            if jogo.fim_do_jogo:
                jogo.printaTabuleiro()
                if jogo.jogador_vencedor == -1 and _jogador_inicial == "H":
                    print("Você Ganhou!")
                if jogo.jogador_vencedor == -1 and _jogador_inicial == "M":
                    print("A Máquina Ganhou! (Cuidado)")
                if jogo.jogador_vencedor == 1 and _jogador_inicial == "H":
                    print("Você Ganhou!")
                if jogo.jogador_vencedor == 1 and _jogador_inicial == "M":
                    print("A Máquina Ganhou! (Cuidado)")
                if jogo.jogador_vencedor == 0:
                    print("EMPATE!!")
                return
            
            os.system('cls' if os.name == 'nt' else 'clear')
        

#testes_print()
main()